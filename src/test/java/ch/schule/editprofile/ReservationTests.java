package ch.schule.editprofile;

import bbw.Restaurant.Reservation;
import bbw.Restaurant.RestaurantApplication;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.Set;

@SpringBootTest(classes=bbw.Restaurant.RestaurantApplication.class)
class ReservationTests {
	private Validator validator;

	@BeforeEach
	public void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	void contextLoads() {
	}


	@Test
	void rejectInvalidAdults() {
		var model = new Reservation();
		model.setEmail("remo.waelchli@blabala.com");
		model.setDate(new Date());
		model.setKids(0);
		model.setLength(2);
		model.setTelNumber("0786665554");

		model.setAdults(-2);

		Set<ConstraintViolation<Reservation>> violations = validator.validate(model);
		Assert.isTrue(violations.size() == 1);

		model.setAdults(2);

		violations = validator.validate(model);
		Assert.isTrue(violations.isEmpty());
	}

	@Test
	void rejectInvalidEmail() {
		var model = new Reservation();
		model.setDate(new Date());
		model.setKids(0);
		model.setAdults(2);
		model.setLength(2);
		model.setTelNumber("0786665554");

		model.setEmail("eeee");

		Set<ConstraintViolation<Reservation>> violations = validator.validate(model);
		Assert.isTrue(violations.size() == 1);

		model.setEmail("remo.waelchli@blabala.com");

		violations = validator.validate(model);
		Assert.isTrue(violations.isEmpty());
	}

}
