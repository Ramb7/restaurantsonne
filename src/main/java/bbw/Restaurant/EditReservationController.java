package bbw.Restaurant;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author luigicavuoti
 *
 */
@Controller
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EditReservationController {
	public ArrayList<Reservation> reservations = new ArrayList<Reservation>();
	static Logger log = Logger.getLogger("EditReservationController");

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String homeForm(Model model) {
		model.addAttribute("reservation", new Reservation());
		return "index";
	}

	@RequestMapping(path = "/editreservation", method = RequestMethod.GET)
	public String editprofileForm(Model model) {
		model.addAttribute("site", "newReservation");
		model.addAttribute("reservation", new Reservation());
		return "editreservation";
	}

	@RequestMapping(path = "/editreservation", method = RequestMethod.POST)
	public String checkPersonInfo(Model model, @ModelAttribute @Valid Reservation reservation, BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			return "editreservation";
		}
		log.info("check person info/post");
		reservations.add(reservation);
		model.addAttribute("reservation", reservation);
		model.addAttribute("reservations", reservations);
		model.addAttribute("site", "reservations");
		return "reservations";
	}

	@RequestMapping(path = "/reservations", method = RequestMethod.GET)
	public String getReservations(Model model) {
		model.addAttribute("site", "reservations");
		model.addAttribute("reservations",reservations);
		return "reservations";
	}

}
