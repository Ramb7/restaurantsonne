package bbw.Restaurant;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.*;
import java.util.Date;

/**
 *
 * @author luigicavuoti
 *
 */
public class Reservation {

	public int getKids() {
		return kids;
	}

	public void setKids(int kids) {
		this.kids = kids;
	}

	public int getAdults() {
		return adults;
	}

	public void setAdults(int adults) {
		this.adults = adults;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@NotNull
	@Min(0)
	private int kids;

	@NotNull
	@Min(1)
	private int adults;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;

	@NotNull
	@Min(1)
	private int length;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	@NotEmpty
	@NotNull
	@Email(message = "Es muss eine gültige Email Adresse sein.")
	private String email;

	@NotNull
	@NotEmpty
	@Pattern( regexp = "[0-9]{10}")
	private String telNumber;

	public String getDateString(){
		return this.getDate().toString();
	}



	@Override
	public String toString() {
		return "Editprofile [kids=" + kids + ", adults=" + adults + ", date=" + date.toString() + "]";
	}

}
